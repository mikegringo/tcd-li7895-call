import numpy as np
import psola

from gtts import gTTS
from math import ceil, floor
from os.path import basename, dirname, join, splitext
from pydub import AudioSegment
from scipy.io import wavfile

def pad_zeros(samples, pad_factor):
    """Add zeros at the end of the array.

    This function adds an array of zeros at the end based on the input array's
    length and a multiplier.

    Parameters
    ----------
    samples : numpy array
        Input array to be padded, of shape (N,)
    pad_factor : int
        Multiplier to be applied on the length of the input array

    Returns
    -------
    padded : numpy array
        Output array, padded with zeros of length pad_factor * len(samples)
    """
    # Determine number of zeros to be padded
    audio_type = samples.dtype
    num_samples = samples.shape[0]
    pad_len = pad_factor * num_samples

    # Generate padded array
    padded = np.pad(samples, (0, pad_len), 'constant')

    return padded

def slow_speech_to_file(input_path, slow_factor, to_pad):
    """Slow rate of speech file

    This function slows the rate of the input speech based on a multiplier.
    Padding zeros is an option for very low (close to zero) multipliers. The
    process is done via the psola Python package.

    Parameters
    ----------
    input_path : string
    slow_factor : float
    to_pad : bool

    Returns
    -------
    none, it writes the slowed speech to a file
    """
    # Save output file in the same directory as the input
    # Add information on slow factor
    output_file = splitext(basename(input_path))[0] \
        + "-psola-snail-talk-" + str(slow_factor) + ".wav"
    output_path = join(dirname(input_path), output_file)

    # Load input
    print("Loading audio file %s..." % input_path)
    fs, samples = wavfile.read(input_path)
    print("Done.")

    # We usually pad zeros if the slow_factor is too low (close to zero) so
    # that the output speech won't cut off
    if to_pad:
        print("Padding audio...")
        pad_factor = ceil(1/slow_factor * 0.5) - 1
        padded = pad_zeros(samples, pad_factor)
        print("Done.")
    else:
        padded = samples

    # Highest frequency should not be greater than half the sampling rate else
    # we'll get aliasing
    print("Synthesizing slowed speech...")
    psola.to_file(audio=padded,
                  sample_rate=fs,
                  output_file=output_path,
                  constant_stretch=slow_factor,
                  fmax=floor(0.5 * fs))
    print("Done. Saved as %s" % output_path)

def run_example():
    """Example to generate "snail talk".

    This function produces a "snail talk" version of the input speech to be
    used in phonemic awareness exercises as a scaffolding tool for learners.
    """
    slow_factors = [0.50, 0.25, 0.10]
    to_pad = True

    sample_word = "bato"
    lang = "tl"
    is_slow = False

    audio_dir = "media"
    input_file = sample_word + ".wav"

    input_path = join(audio_dir, input_file)

    synthesize_speech(sample_word, lang, input_path, is_slow)
    # slow_speech_to_file(input_path, 0.50, to_pad)
    [slow_speech_to_file(input_path, factor, to_pad) for factor in slow_factors]

def synthesize_speech(text, lang, output_path, is_slow=False):
    """Generate speech from text.

    This function synthesizes speech corresponding to an input text from a
    supported language. This function uses the gTTS package. To see all
    languages suported by the package, type in gtts-cli --all on the command
    line.

    Parameters
    ----------
    text : string
        Input text to be synthesized
    lang : string
        Language code. Type in gtts-cli --all on the command line to see all
        codes accepted by gTTS
    output_path : string
        Location and file name of the output file
    is_slow : bool
        Setting to use the slow feature of gTTS. By default it is set to False.

    Returns
    -------
    none, it writes the synthesized speech to a file
    """
    mp3_path = output_path.replace(".wav",".mp3")
    synth_obj = gTTS(text=text,
                     lang=lang,
                     slow=is_slow)
    synth_obj.save(mp3_path)

    audio = AudioSegment.from_mp3(mp3_path)
    audio.export(output_path, format="wav")

def main():
    run_example()

if __name__ == "__main__":
    main()
